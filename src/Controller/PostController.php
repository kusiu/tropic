<?php

namespace App\Controller;

use App\Service\PostAdapter;
use App\Service\PostService;
use App\Service\PostServiceInterface;
use App\Service\UserService;
use App\Service\UserServiceInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class PostController
{
    private PostServiceInterface $postService;
    private UserServiceInterface $userService;
    private PostAdapter $postAdapter;

    public function __construct(PostService $postService, UserService $userService)
    {
        $this->postService = $postService;
        $this->userService = $userService;
        $this->postAdapter = new PostAdapter($this->postService, $this->userService);;
    }

    /**
     * @Route("/posts", name="app_posts")
     */
    public function list(Request $request)
    {
        try {
            $result = $this->postAdapter->getUsersPosts($request->get('filters') ?? []);
            return new JsonResponse($result);
        } catch (\Exception $exception) {
            // log error
            return new JsonResponse(['message' => 'Internal error'], 500);
        }
    }
}
