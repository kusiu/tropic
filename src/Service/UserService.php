<?php

namespace App\Service;

class UserService extends ClientService implements UserServiceInterface
{
    public function getUsers(): array
    {
        $response = $this->client->get('users');
        return json_decode($response->getBody()->getContents(), true);
    }
}
