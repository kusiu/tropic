<?php

namespace App\Service;

interface UserServiceInterface
{
    public function getUsers(): array;
}
