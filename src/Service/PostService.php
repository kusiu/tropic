<?php

namespace App\Service;

class PostService extends ClientService implements PostServiceInterface
{
    public function getPosts(): array
    {
        $response = $this->client->get('todos');
        return json_decode($response->getBody()->getContents(), true);
    }
}
