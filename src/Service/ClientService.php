<?php

namespace App\Service;

use GuzzleHttp\Client;

abstract class ClientService
{
    protected Client $client;

    public function __construct()
    {
        $this->client = new Client(['base_uri' => 'https://jsonplaceholder.typicode.com/']);
    }
}
