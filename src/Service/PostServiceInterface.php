<?php

namespace App\Service;

interface PostServiceInterface
{
    public function getPosts(): array;
}
