<?php

namespace App\Service;

class PostAdapter
{
    private PostServiceInterface $postService;
    private UserServiceInterface $userService;

    public function __construct(PostServiceInterface $postService, UserServiceInterface $userService)
    {
        $this->postService = $postService;
        $this->userService = $userService;
    }

    public function getUsersPosts(array $filters = []): array
    {
        $posts = $this->postService->getPosts();
        $users = $this->userService->getUsers();

        $result = [];
        foreach ($posts as $post) {
            foreach ($users as $user) {
                if ($user['id'] == $post['userId']) {
                    $userIndex['user'] = $user;
                    $result[] = array_merge($post, $userIndex);
                }
            }
        }

        if ($filters) {
            $result = $this->filterResults($filters, $result);
        }

        return $result;
    }

    private function filterResults(array $filters, array $result): array
    {
        $filteredResult = [];

        if (isset($filters['title'])) {
            $search = $filters['title'];
            $filteredResult = array_filter($result, function($v, $k) use ($search) {
                return $v['title'] == $search;
            }, ARRAY_FILTER_USE_BOTH);
        }

        // todo: extend filter by other fields

        return $filteredResult;
    }
}
