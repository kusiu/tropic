<?php

namespace Test\Service;

use App\Service\PostAdapter;
use App\Service\PostService;
use App\Service\UserService;
use PHPUnit\Framework\TestCase;

class PostAdapterTest extends TestCase
{
    private PostAdapter $postAdapter;

    protected function setUp()
    {
        parent::setUp();

        $postService = $this->createMock(PostService::class);
        $postService
            ->method('getPosts')
            ->willReturn([
                [
                    'userId' => 1,
                    'id' => 1,
                    'title' => 'delectus aut autem',
                    'completed' => false,
                ],
                [
                    'userId' => 1,
                    'id' => 2,
                    'title' => 'quis ut nam facilis et officia qui',
                    'completed' => false,
                ],
                [
                    "userId" => 2,
                    "id" => 21,
                    "title" => "suscipit repellat esse quibusdam voluptatem incidunt",
                    "completed" => false
                ],
                [
                    "userId" => 2,
                    "id" => 22,
                    "title" => "distinctio vitae autem nihil ut molestias quo",
                    "completed" => true
                ]
            ]);

        $userService = $this->createMock(UserService::class);
        $userService
            ->method('getUsers')
            ->willReturn([
                [
                    'id' => 1,
                    'name' => 'Leanne Graham',
                    'username' => 'Bret',
                    'email' => 'Sincere@april.biz',
                    'address' =>
                        [
                            'street' => 'Kulas Light',
                            'suite' => 'Apt. 556',
                            'city' => 'Gwenborough',
                            'zipcode' => '92998-3874',
                            'geo' =>
                                [
                                    'lat' => '-37.3159',
                                    'lng' => '81.1496',
                                ],
                        ],
                    'phone' => '1-770-736-8031 x56442',
                    'website' => 'hildegard.org',
                    'company' =>
                        [
                            'name' => 'Romaguera-Crona',
                            'catchPhrase' => 'Multi-layered client-server neural-net',
                            'bs' => 'harness real-time e-markets',
                        ],
                ],
                [
                    'id' => 2,
                    'name' => 'Ervin Howell',
                    'username' => 'Antonette',
                    'email' => 'Shanna@melissa.tv',
                    'address' =>
                        [
                            'street' => 'Victor Plains',
                            'suite' => 'Suite 879',
                            'city' => 'Wisokyburgh',
                            'zipcode' => '90566-7771',
                            'geo' =>
                                [
                                    'lat' => '-43.9509',
                                    'lng' => '-34.4618',
                                ],
                        ],
                    'phone' => '010-692-6593 x09125',
                    'website' => 'anastasia.net',
                    'company' =>
                        [
                            'name' => 'Deckow-Crist',
                            'catchPhrase' => 'Proactive didactic contingency',
                            'bs' => 'synergize scalable supply-chains',
                        ],
                ],
            ]);

        $this->postAdapter = new PostAdapter($postService, $userService);
    }

    /** @test */
    public function list_of_users_is_merged_into_list_of_posts()
    {
        $posts = $this->postAdapter->getUsersPosts();
        $this->assertIsArray($posts);
        $this->assertCount(4, $posts);
        $this->assertArrayHasKey('userId', $posts[0]);
        $this->assertArrayHasKey('id', $posts[0]);
        $this->assertArrayHasKey('title', $posts[0]);
        $this->assertArrayHasKey('completed', $posts[0]);
        $this->assertArrayHasKey('user', $posts[0]);
    }

    /** @test */
    public function filter_posts_by_title()
    {
        $posts = $this->postAdapter->getUsersPosts(['title' => 'delectus aut autem']);
        $this->assertCount(1, $posts);
        $this->assertEquals('delectus aut autem', $posts[0]['title']);
    }

    /** @test */
    public function filter_posts_by_non_existing_title()
    {
        $posts = $this->postAdapter->getUsersPosts(['title' => 'delectus aut autem asdaas']);
        $this->assertCount(0, $posts);
    }
}
