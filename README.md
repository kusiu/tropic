# Tropic Challange

API endpoint that displays a list of todos and their associated user information 

## Requirement
PHP 7.4

Composer

## Installation

Download Symfony: https://symfony.com/download

```sh
composer install

symfony server:start
```

## Usage
List of all posts:

http://127.0.0.1:8000/posts

Filter posts by title:

http://127.0.0.1:8000/posts?filters[title]=sunt cum tempora

## Run Unit Tests

```sh
php bin/phpunit
```
