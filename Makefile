export SHELL = /bin/bash
PWD = $(shell pwd)

default: help

help: ## The help text you're reading
	@grep --no-filename -E '^[a-zA-Z1-9_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
.PHONY: help

up: ## Bring your local development environment up
	make install run composer-install
	docker-compose exec app php artisan config:cache
.PHONY: up

install: ## Install environment
	docker-compose build --no-cache
.PHONY: install

run: ## Run application in docker containers
	docker-compose up -d --remove-orphans
.PHONY: run

down: ## Removes all images
	docker-compose down
.PHONY: down

composer-install: ## Install composer
	docker-compose exec app composer install
.PHONY: composer-install

migrate-rollback: ## Run migration rollback
	docker-compose exec app php artisan migrate:rollback
.PHONY: migrate-rollback

ssh: ## Run app shell
	docker-compose exec app bash
.PHONY: ssh

tests: ## Run tests
	docker-compose exec app ./vendor/bin/phpunit --exclude-group exigo --stop-on-failure
.PHONY: tests

dump-autoload: ## Dump autoload
	docker-compose exec app composer dump-autoload
.PHONY: dump-autoload

laravel-logs: ## Tail Laravel logs
	docker-compose exec app tail -f ./storage/logs/laravel.log
.PHONY: laravel-logs

clear-cache: ## Clear cache in development environment
	@echo "Clearing cache"
	php artisan cache:clear && php artisan route:cache && php artisan config:cache && php artisan view:clear && php artisan config:clear
	@echo "Done"
.PHONY: clear-cache

clear-config-cache: ## Clear config cache
	php artisan config:clear
.PHONY: clear-cache
